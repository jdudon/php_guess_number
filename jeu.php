<?php
session_start();

$limits = ['min' => 0, 'max' => 100];
if (empty($_SESSION['number'])) {

    $_SESSION['number'] = rand($limits['min'], $limits['max']);
    $_SESSION['try'] = 0;
}

$number = $_SESSION['number'];
$try = $_SESSION['try'];
if ($_SESSION['try'] === 5) {
 echo "perdu";
 unset($_SESSION['number'], $number);
 unset($_SESSION['try'], $try);
} else {
if (isset($_POST)) {
    if (isset($_POST['chiffre']) and !empty($_POST['chiffre'])) {

        $chiffre = intval($_POST['chiffre']);

        if ($chiffre > $number) {
            $message = 'Trop haut';
            $_SESSION['try']= $try+1;
            echo $try;
            echo "<br>";
            echo $number;
        } elseif ($chiffre < $number) {
            $message = 'Trop bas';
            $_SESSION['try']= $try+1;
            echo $try;
            echo "<br>";
            echo $number;
        } else {
            $message = 'Vous avez trouvé le bon chiffre !';
            unset($_SESSION['number'], $number);
            unset($_SESSION['try']);

        }
    } else {

        $message = "Veuillez introduire un chiffre !";
    }
}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php echo $message; ?>
    <form action="" method="POST">
        <label for="chiffre">Chiffre entre 0 et 100:</label>
        <input type="number" name="chiffre" min="0" max="100">
        <button type="submit">Essayer</button>
    </form>
</body>

</html>